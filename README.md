# Customized redis:alpine Docker Image for Pimcore 5

## Notes

* the image uses a config suitable for pimcore 5 
* this image is optimized to be used with `basilicom/php-7-fpm-pimcore`
